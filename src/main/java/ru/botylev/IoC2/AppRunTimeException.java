package ru.botylev.IoC2;


public class AppRunTimeException extends RuntimeException {
    public AppRunTimeException(String message) {
        super(message);
    }
}
