package ru.botylev.IoC2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.Arrays;


public class Main13_2 {
    public static void main(String[] args) throws IOException, IllegalAccessException, NoSuchFieldException, ClassNotFoundException, InstantiationException {
        ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
//        MyObject myObj = new MyObject();
        MyObject myObj = (MyObject) context.getBean("MyObject");
        myObj.myInt = 324243;
        myObj.myString = "This is a test string";

//        ObjectWriter objWriter = new ObjectWriter();
        ObjectWriter objWriter = (ObjectWriter) context.getBean("ObjectWriter");
        objWriter.SaveToFile(myObj, "d://JAVA_DEBUG//res2.txt");

        Object myObj2 = objWriter.LoadFromFile("d://JAVA_DEBUG//res2.txt");
        System.out.println(myObj2.toString());

        // проверка содержимого контекста
        System.out.println(Arrays.toString(context.getBeanDefinitionNames()));
    }
}