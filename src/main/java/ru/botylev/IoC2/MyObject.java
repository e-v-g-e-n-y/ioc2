package ru.botylev.IoC2;

public class MyObject {
    public int myInt;
    public boolean myBool;
    public String myString;

        @Override
    public String toString() {
        return "MyObject{" +
                "myInt=" + myInt +
                ", myBool=" + myBool +
                ", MyString='" + myString + '\'' +
                '}';
    }
}
