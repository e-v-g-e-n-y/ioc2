package ru.botylev.IoC2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;

public class ObjectWriter {
    private final String classNameProperty = "Object.class";
    private final String typeNameProperty = ".type";
    private final String valueNameProperty = ".value";
    public void SaveToFile(Object pObject, String pFileName) throws IllegalAccessException {
        StringBuilder content = new StringBuilder();

        System.out.println(pObject.getClass().getName());
        Properties properties = new Properties();
        properties.setProperty(classNameProperty, pObject.getClass().getName());
        for (Field aField: pObject.getClass().getFields()){
            properties.setProperty(aField.getName() + typeNameProperty, aField.getType().toString());
            properties.setProperty(aField.getName() + valueNameProperty, aField.get(pObject).toString());
        }
        System.out.println(properties.toString());
        try (FileOutputStream fileOutputStream = new FileOutputStream(pFileName)) {

            properties.store(fileOutputStream, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Object LoadFromFile(String fileName) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, InstantiationException, IOException {
        Properties properties = new Properties();
        try (FileInputStream fileInputStream = new FileInputStream(fileName)) {
            properties.load(fileInputStream);
        }
        if (properties.containsKey(classNameProperty) == false) {
            throw new AppRunTimeException("Не удалось найти имя класса в загружаемом файле");
        }
        String className = properties.getProperty(classNameProperty);
        ClassLoader classLoader = MyObject.class.getClassLoader();
        Class vClass = classLoader.loadClass(className);
        Object objResult = vClass.newInstance();
        // пребор и заполнение полей целевого класса
        for (Field field: vClass.getFields()){
            if ((properties.containsKey(field.getName() + valueNameProperty))&&(properties.containsKey(field.getName() + typeNameProperty))) {
                String recClassName =properties.getProperty(field.getName() + typeNameProperty);
                String fieldClassName = field.getType().toString();
                Class fieldType = field.getType();
                if (! fieldClassName.equals(recClassName)) {
                    throw new AppRunTimeException("Несоответствие типов полей класса и сохранённой в записи."
                            +" Fieldname='" + field.getName() + "':" + fieldClassName + "<>" + recClassName);
                }
                field.set(objResult, parseFieldValueByType(fieldType, properties.getProperty(field.getName() + valueNameProperty)));
            }
        }
        return objResult;
    }
    protected static Object parseFieldValueByType(Class fieldType, String value)  {
        if (fieldType.toString().equals("int"))     return (int) Integer.parseInt(value);
        if (fieldType.toString().equals("boolean")) return Boolean.parseBoolean(value);
        if (fieldType.toString().equals("double"))  return (double) Double.parseDouble(value);
        if (fieldType == Integer.class)             return Integer.parseInt(value);
        if (fieldType == Byte.class)                return Byte.parseByte(value);
        if (fieldType == Short.class)               return Short.parseShort(value);
        if (fieldType == Long.class)                return Long.parseLong(value);
        if (fieldType == Boolean.class)             return Boolean.parseBoolean(value);
        if (fieldType == Float.class)               return Float.parseFloat(value);
        if (fieldType == Double.class)              return Double.parseDouble(value);
        return value;
    }

}
